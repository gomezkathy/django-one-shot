from django.shortcuts import render, redirect
from todos.models import TodoList
from todos.forms import TodoListForm
# Create your views here.

def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list_list": todos,
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request,id):
    item = TodoList.objects.get(id=id)
    context = {
        "todo_list_detail": item,
    }

    return render(request, "todos/detail.html", context)

def todo_list_create(request):
    if request.method=="POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            create = form.save()
            return redirect("todo_list_detail",id=create.id)
    else:
        form = TodoListForm()

    context = {
        "form": form
    }
    return render(request,"todos/create.html", context)


def todo_list_update(request, id):
    updates = TodoList.objects.get(id=id)

    if request.method == "POST":
        form = TodoListForm(request.POST, instance=updates)

        if form.is_valid():
            updates = form.save()
            return redirect("todo_list_detail",id=updates.id)
    else:
        form = TodoListForm(instance=updates)
    context = {
        "form": form
    }
    return render(request,"todos/edit.html", context)
